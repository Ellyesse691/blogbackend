<?php

// use App\Kernel;

// require_once dirname(__DIR__).'/vendor/autoload_runtime.php';

// return function (array $context) {
//     return new Kernel($context['APP_ENV'], (bool) $context['APP_DEBUG']);
// };

use App\Entities\Comment;
use App\Entities\Post;
use App\Repository\CommentRepository;
use App\Repository\PostRepository;

require '../vendor/autoload.php';

$repoPost = new PostRepository();
$repoComment = new CommentRepository();

var_dump($repoPost->findAll());
var_dump($repoPost->deleteById(2));
