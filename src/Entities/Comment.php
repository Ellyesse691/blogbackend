<?php

namespace App\Entities;
use DateTime;

class Comment{
    private ?int $id;
    private ?string $nom;
    private ?string $commentaire;
    private ?DateTime $date;

    /**
     * @param int|null $id
     * @param string|null $nom
     * @param string|null $commentaire
     * @param DateTime|null $date
     */
    public function __construct(?int $id, ?string $nom, ?string $commentaire, ?DateTime $date) {
    	$this->id = $id;
    	$this->nom = $nom;
    	$this->commentaire = $commentaire;
    	$this->date = $date;
    }

	/**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getNom(): ?string {
		return $this->nom;
	}
	
	/**
	 * @param string|null $nom 
	 * @return self
	 */
	public function setNom(?string $nom): self {
		$this->nom = $nom;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getCommentaire(): ?string {
		return $this->commentaire;
	}
	
	/**
	 * @param string|null $commentaire 
	 * @return self
	 */
	public function setCommentaire(?string $commentaire): self {
		$this->commentaire = $commentaire;
		return $this;
	}
	
	/**
	 * @return DateTime|null
	 */
	public function getDate(): ?DateTime {
		return $this->date;
	}
	
	/**
	 * @param DateTime|null $date 
	 * @return self
	 */
	public function setDate(?DateTime $date): self {
		$this->date = $date;
		return $this;
	}
}