<?php

namespace App\Entities;
use DateTime;

class Post {
    private ?int $id;
    private ?string $pseudo;
    private ?string $image;
    private ?string $titre;
    private ?string $description;
    private ?DateTime $date;

    /**
     * @param int|null $id
     * @param string|null $pseudo
     * @param string|null $image
     * @param string|null $titre
     * @param string|null $description
     * @param DateTime|null $date
     */
    public function __construct(?string $pseudo, ?string $image, ?string $titre, ?string $description, ?DateTime $date ,?int $id = null) {
    	$this->id = $id;
    	$this->pseudo = $pseudo;
    	$this->image = $image;
    	$this->titre = $titre;
    	$this->description = $description;
    	$this->date = $date;
    }

	/**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getPseudo(): ?string {
		return $this->pseudo;
	}
	
	/**
	 * @param string|null $pseudo 
	 * @return self
	 */
	public function setPseudo(?string $pseudo): self {
		$this->pseudo = $pseudo;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getImage(): ?string {
		return $this->image;
	}
	
	/**
	 * @param string|null $image 
	 * @return self
	 */
	public function setImage(?string $image): self {
		$this->image = $image;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getTitre(): ?string {
		return $this->titre;
	}
	
	/**
	 * @param string|null $titre 
	 * @return self
	 */
	public function setTitre(?string $titre): self {
		$this->titre = $titre;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getDescription(): ?string {
		return $this->description;
	}
	
	/**
	 * @param string|null $description 
	 * @return self
	 */
	public function setDescription(?string $description): self {
		$this->description = $description;
		return $this;
	}
	
	/**
	 * @return DateTime|null
	 */
	public function getDate(): ?DateTime {
		return $this->date;
	}
	
	/**
	 * @param DateTime|null $date 
	 * @return self
	 */
	public function setDate(?DateTime $date): self {
		$this->date = $date;
		return $this;
	}
}