<?php

namespace App\Controller;
use App\Repository\CommentRepository;
use App\Repository\Comment;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Response;

#[Route('/api/comment')]
class CommentController extends AbstractController{

    public CommentRepository $repo;

    public function __construct(CommentRepository $repo) {
    	$this->repo = $repo;
    }

    public function all() {
        
        $comments = $this->repo->findAll();
        return $this->json($comments);
    }

    #[Route('/{id}' , methods: 'GET')]
    public function one(int $id) {
        $comment = $this->repo->findById($id);
        if(!$comment){
            throw new NotFoundHttpException();

        }
        return $this->json($comment);
    }

    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer) {
        $comment = $serializer->deserialize($request->getContent(), Comment::class, 'json');
        $this->repo->persist($comment);

        return $this->json($comment , Response::HTTP_CREATED);
    }

    #[Route('/{id}' , methods: 'DELETE')]
    public function deleteById(Request $request, SerializerInterface $serializer, int $id) {
        $comment = $this->repo->findById($id);

        if (!$comment) {
            $this->json(['message' => 'commentez'], Response::HTTP_NO_CONTENT);
        }

        $this->repo->deleteById($comment);
        return $this->json($comment);
    }

}