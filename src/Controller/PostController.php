<?php

namespace App\Controller;
use App\Repository\PostRepository;
use App\Repository\Post;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Response;

#[Route('/api/post')]
class PostController extends AbstractController{

    public PostRepository $repo;

    public function __construct(PostRepository $repo) {
    	$this->repo = $repo;
    }

    public function all() {
        
        $post = $this->repo->findAll();
        return $this->json($post);
    }

    #[Route('/{id}' , methods: 'GET')]
    public function one(int $id) {
        $post = $this->repo->findById($id);
        if(!$post){
            throw new NotFoundHttpException();

        }
        return $this->json($post);
    }

    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer) {
        $post = $serializer->deserialize($request->getContent(), Comment::class, 'json');
        $this->repo->persist($post);

        return $this->json($post , Response::HTTP_CREATED);
    }

    #[Route('/{id}' , methods: 'DELETE')]
    public function deleteById(Request $request, SerializerInterface $serializer, int $id) {
        $post = $this->repo->findById($id);

        if (!$post) {
            $this->json(['message' => 'voici le post'], Response::HTTP_NO_CONTENT);
        }

        $this->repo->deleteById($post);
        return $this->json($post);
    }

}