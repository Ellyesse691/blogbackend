<?php

namespace App\Repository;
use App\Entities\Comment;
use DateTime;
use PDO;

class CommentRepository{
    private PDO $connection;
    
    public function __construct()
    {
        $this->connection = Database::connect();
    }

    public function findAll(){
        $comments = [];

        $statment = $this->connection->prepare('SELECT * FROM Comment');
        $statment->execute();

        foreach($statment->fetchAll() as $item){
            $date = null;
        if(isset($item['date'])){
            $date = new DateTime($item['date']);
        }
            $comments[] = new Comment($item['nom'], $item['commentaire'], $date, $item['id']);
        }
        
        return $comments;
    }


    public function findById(int $id):?Comment {
        $statement = $this->connection->prepare('SELECT * FROM Comment WHERE id=:id');
        $statement->bindValue('id', $id);

        $statement->execute();

        $result = $statement->fetch();
        if($result) {
            return $this->sqlToComment($result);
        }
        return null;
    }

    public function sqlToComment(array $item):Comment {
        $date = null;
        if(isset($item['date'])){
            $date = new DateTime($item['date']);
        }
        return new Comment($item['nom'], $item['commentaire'], $date, $item['id']);
    }



    public function persist(Comment $comment){
        
        $statement = $this->connection->prepare('INSERT INTO Comment(nom, commentaire, date) VALUES (:nom,:commentaire,:titre,:date)');

        $statement->execute([
            'nom'  => $comment->getNom(),
            'commentaire'  => $comment->getCommentaire(),
            'date'  => $comment->getDate()
        ]);
        $comment->setId($this->connection->lastInsertId());
    }

    public function update(Comment $comment): void
    {
        $query = $this->connection->prepare("UPDATE Post SET pseudo=:pseudo, image = :image, titre=:titre, description=:description, date=:date WHERE id=:id");
        
        $query->bindValue(":nom", $comment->getNom(), PDO::PARAM_STR);
        $query->bindValue(":commentaire", $comment->getCommentaire(), PDO::PARAM_STR);
        $query->bindValue(":date", $comment->getDate()->format('Y-m-d'));
        $query->bindValue(":id", $comment->getId(), PDO::PARAM_INT);
        
        $query->execute();
    }

    public function deleteById(int $id)
    {
        $statement = $this->connection->prepare("DELETE FROM Comment WHERE id=:id");
        $statement->bindValue('id', $id);
        
        $statement->execute();
        
    }
}
