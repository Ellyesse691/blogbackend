<?php

namespace App\Repository;
use App\Entities\Post;
use DateTime;
use PDO;

class PostRepository{
    private PDO $connection;
    
    public function __construct()
    {
        $this->connection = Database::connect();
    }

    public function findAll(){
        $post = [];

        $statment = $this->connection->prepare('SELECT * FROM Post');
        $statment->execute();

        

        foreach($statment->fetchAll() as $item){
            $date = null;
        if(isset($item['date'])){
            $date = new DateTime($item['date']);
        }
            $post[] = new Post($item['pseudo'], $item['image'], $item['titre'], $item['description'], $date, $item['id']);
        }
        
        return $post;
    }


    public function findById(int $id):?Post {
        $statement = $this->connection->prepare('SELECT * FROM Post WHERE id=:id');
        $statement->bindValue('id', $id);

        $statement->execute();

        $result = $statement->fetch();
        if($result) {
            return $this->sqlToPost($result);
        }
        return null;
    }

    public function sqlToPost(array $item):Post {
        $date = null;
        if(isset($item['date'])){
            $date = new DateTime($item['date']);
        }
        return new Post($item['pseudo'], $item['image'], $item['titre'], $item['description'], $date, $item['id']);
    }

    public function persist(Post $post){
        
        $statement = $this->connection->prepare('INSERT INTO Post(pseudo, image, titre, description, date) VALUES (:pseudo,:image,:titre,:description,:date)');

        $statement->execute([
            'pseudo'  => $post->getPseudo(),
            'image'  => $post->getImage(),
            'titre' => $post->getTitre(),
            'description'  => $post->getDescription(),
            'date'  => $post->getDate()
        ]);
        $post->setId($this->connection->lastInsertId());
    }

    public function update(Post $post): void
    {
        $query = $this->connection->prepare("UPDATE Post SET pseudo=:pseudo, image = :image, titre=:titre, description=:description, date=:date WHERE id=:id");
        
        $query->bindValue(":pseudo", $post->getPseudo(), PDO::PARAM_STR);
        $query->bindValue(":image", $post->getImage(), PDO::PARAM_STR);
        $query->bindValue(":titre", $post->getTitre(), PDO::PARAM_STR);
        $query->bindValue(":description", $post->getDescription(), PDO::PARAM_STR);
        $query->bindValue(":date", $post->getDate()->format('Y-m-d'));
        $query->bindValue(":id", $post->getId(), PDO::PARAM_INT);
        
        $query->execute();
    }

    public function deleteById(int $id)
    {
        $statement = $this->connection->prepare("DELETE FROM Post WHERE id=:id");
        $statement->bindValue('id', $id);
        
        $statement->execute();
        
    }
}