-- Active: 1674723547429@@127.0.0.1@3306@ProjetBlog

DROP TABLE IF EXISTS Comment;

DROP TABLE IF EXISTS Post;

CREATE TABLE
    Comment (
        id INT PRIMARY KEY AUTO_INCREMENT,
        nom VARCHAR(50) NOT NULL,
        commentaire VARCHAR(1000),
        date DATETIME
    );

CREATE TABLE
    Post (
        id INT PRIMARY KEY AUTO_INCREMENT,
        pseudo VARCHAR(255) NOT NULL,
        image VARCHAR(255) ,
        titre VARCHAR(255) ,
        description VARCHAR(1000) ,
        date DATETIME
    );